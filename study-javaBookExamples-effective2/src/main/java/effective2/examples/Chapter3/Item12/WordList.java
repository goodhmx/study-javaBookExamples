package effective2.examples.Chapter3.Item12;

import java.math.BigDecimal;
import java.util.*;

public class WordList {
    public static void main(String[] args) {
        Set<String> s = new TreeSet<String>();
        Collections.addAll(s, args);
        System.out.println(s);
        Set<BigDecimal> ts=new TreeSet<BigDecimal>();
        ts.add(new BigDecimal("1.0"));
        ts.add(new BigDecimal("1.00"));
        System.out.println(ts);
        Set<BigDecimal> hs=new HashSet<BigDecimal>();
        hs.add(new BigDecimal("1.0"));
        hs.add(new BigDecimal("1.00"));
        System.out.println(hs);
        
    }
}

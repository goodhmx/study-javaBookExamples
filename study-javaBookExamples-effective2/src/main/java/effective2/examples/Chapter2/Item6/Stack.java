package effective2.examples.Chapter2.Item6;

// Can you spot the "memory leak"?

import java.util.*;

public class Stack {
    private Object[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;

    public Stack() {
        elements = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public void push(Object e) {
        ensureCapacity();
        elements[size++] = e;
    }

    public Object pop() {
        if (size == 0)
            throw new EmptyStackException();
        return elements[--size];
    }
    public Object pop2() {
        if (size == 0)
            throw new EmptyStackException();
        Object o=elements[--size];
        elements[size]=null;
        return o;
    }
    public static void 打印(){
        System.out.println("中文方法");
    }
    /**
     * Ensure space for at least one more element, roughly
     * doubling the capacity each time the array needs to grow.
     */
    private void ensureCapacity() {
        if (elements.length == size)
            elements = Arrays.copyOf(elements, 2 * size + 1);
    }
    public static void main(String args[]){
        打印();
    	Stack s=new Stack();
    	s.push(new String("1"));
    	s.push(new String("2"));
    	s.pop2();
    	String str=(String)s.pop2();
    	
    	System.out.println("size"+s.size);
    	System.out.println("str:"+str);

    }
}

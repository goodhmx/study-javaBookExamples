package v1ch14.forkJoin;

public class NonRecursive {
	public static void main(String[] args) {
		final int SIZE = 100000000;
		double[] numbers = new double[SIZE];

		int count = 0;
		for (int i = 0; i < SIZE; i++) {
			numbers[i] = Math.random();
			if (numbers[i] > 0.5)
				count++;
		}
		System.out.println(count);

	}
}

package v1ch14.single;

/**
 * This program shows data corruption when multiple threads access a data structure.
 * @version 1.30 2004-08-01
 * @author Cay Horstmann
 */
public class SingleBankTest
{
   public static final int NACCOUNTS = 100;
   public static final double INITIAL_BALANCE = 1000;

   public static void main(String[] args)
   {
	  long start = System.currentTimeMillis();
  
      Bank b = new Bank(NACCOUNTS, INITIAL_BALANCE);
      int from=0;
      
      while(System.currentTimeMillis()<(start+1000)){ 	  
          int toAccount = (int) (b.size() * Math.random());
          from = toAccount >0 ? (toAccount-1) : 99 ;
          double amount = INITIAL_BALANCE * Math.random();
          b.transfer(from, toAccount, amount);
      }
	  System.exit(0);
      
   }
}

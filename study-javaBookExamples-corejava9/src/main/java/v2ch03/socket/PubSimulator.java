package v2ch03.socket;


import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Scanner;

public class PubSimulator {
	   public static void main(String[] args) throws IOException
	   {
		      Properties props = new Properties();
		      try (InputStream in = Files.newInputStream(Paths.get("pub.properties")))
		      {
		         props.load(in);
		      }
		      String logFile = props.getProperty("logFile").toString();
		      String charSet = props.getProperty("charSet").toString();
		      int fileSize = Integer.parseInt(props.getProperty("fileSize").toString());
		      int socketSendBufferSize = Integer.parseInt(props.getProperty("socketSendBufferSize").toString());
		      int streamBufferLenth_SND=Integer.parseInt(props.getProperty("streamBufferLenth_SND").toString());

		      System.out.println(logFile);
		   
	      try 
	      {
		      // establish server socket
	    	  ServerSocket s = new ServerSocket(8189);
	         // wait for client connection
	    	  while(true){
	    		  Socket incoming = s.accept();
				if (socketSendBufferSize > 0)
					incoming.setSendBufferSize(socketSendBufferSize);
		    	  Runnable r= new SenderHandlerBinary2(incoming,logFile,charSet,fileSize,streamBufferLenth_SND);		    	  
		    	  Thread t= new Thread(r);
		    	  t.start();
		    	  
	    	  }

	         }catch(IOException e){
	        	 e.printStackTrace();
	         }
	         
	   }
}
class Sender implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	
	public  Sender(Socket s,String f,String c){
		incoming=s;
		fileName=f;
		charSet=c;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {
				Scanner in = new Scanner(new FileInputStream(fileName),charSet);			
				PrintWriter out = new PrintWriter(incoming.getOutputStream(),true);

				boolean done = false;
				while (!done && in.hasNextLine()) {
					String line = in.nextLine();
					System.out.println(line);
					out.println(line);
					if (line.trim().equals("BYE"))
						done = true;
				}

			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
class SenderHandlerBinary implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	private int fileSize;

	
	public  SenderHandlerBinary(Socket s,String f,String c,int size){
		incoming=s;
		fileName=f;
		charSet=c;
		fileSize=size;

	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {
				InputStream in =new FileInputStream(fileName);
//				OutputStream out = incoming.getOutputStream();
                System.out.println("...BufferedOutputStream...");
				BufferedOutputStream out = new BufferedOutputStream(incoming.getOutputStream());
               
			    byte[] data = new byte[fileSize];
			      int offset = 0;
			      while (offset < fileSize) {
			         int bytesRead = in.read(data, offset, data.length - offset);
			         if (bytesRead == -1) break;
			         offset += bytesRead;
			      }
				
//				int i=0;
//				while(i!=-1)
//				{
//					i=in.read();
//					out.write(i);
//					
//				}
			    out.write(data);
				in.close();
                out.close();

			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
class SenderHandlerBinary2 implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	private int fileSize;
	private int streamBufferLenth;

	
	public  SenderHandlerBinary2(Socket s,String f,String c,int size,int sbl){
		incoming=s;
		fileName=f;
		charSet=c;
		fileSize=size;
		streamBufferLenth=sbl;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {
				InputStream in =new FileInputStream(fileName);
//				OutputStream out = incoming.getOutputStream();
                System.out.println("BufferedOutputStream");
				BufferedOutputStream out = new BufferedOutputStream(incoming.getOutputStream());
               
			    byte[] data = new byte[streamBufferLenth];
			    int bytesRead=0;
			    while ((bytesRead = in.read(data))!=-1) {
			    	out.write(data, 0, bytesRead);
			      }
				
				in.close();
                out.close();

			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}


package v2ch03.socket;


import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Scanner;

public class Receiver {
	   public static void main(String[] args) throws IOException
	   {
		      Properties props = new Properties();
		      try (InputStream in = Files.newInputStream(Paths.get("pub.properties")))
		      {
		         props.load(in);
		      }
		      String logFile = props.getProperty("logFile").toString();
		      String charSet = props.getProperty("charSet").toString();
		      String serverIP = props.getProperty("serverIP").toString();
		      int fileSize = Integer.parseInt(props.getProperty("fileSize").toString());
		      int socketReceiveBufferSize = Integer.parseInt(props.getProperty("socketReceiveBufferSize").toString());
		      int streamBufferLenth_RCV=Integer.parseInt(props.getProperty("streamBufferLenth_RCV").toString());
		      System.out.println(logFile);
		   
	      try 
	      {
		      // establish server socket
//	    	  ServerSocket s = new ServerSocket(8189);
	         // wait for client connection
	    	  Socket s = new Socket(serverIP,8189);
	    	  if(socketReceiveBufferSize>0)
              s.setReceiveBufferSize(socketReceiveBufferSize);
//	    	  while(true){
//	    		  Socket incoming = s.accept();
//		    	  Runnable r= new ReceiverHandler(incoming,logFile,charSet);		    
		    	  Runnable r= new ReceiverHandlerBinary2(s,logFile,charSet,fileSize,streamBufferLenth_RCV);		    	  

		    	  Thread t= new Thread(r);
		    	  t.start();
		    	  
//		    	  Runnable r2= new ReceiverHandlerBinary2(s,logFile+2,charSet,fileSize,streamBufferLenth_RCV);		    	  
//
//		    	  Thread t2= new Thread(r2);
//		    	  t2.start();
		    	  
//	    	  }

	         }catch(IOException e){
	        	 e.printStackTrace();
	         }
	         
	   }
}
class ReceiverHandler implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	
	public  ReceiverHandler(Socket s,String f,String c){
		incoming=s;
		fileName=f;
		charSet=c;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {

				InputStream inStream = incoming.getInputStream();
				PrintWriter out = new PrintWriter(fileName,charSet);
				Scanner in = new Scanner(inStream);

				boolean done = false;
				while (!done && in.hasNextLine()) {
					String line = in.nextLine();
					out.println(line);
					out.flush();
					if (line.trim().equals("BYE"))
						done = true;
				}

			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
class ReceiverHandlerBinary implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	private int fileSize;
	public  ReceiverHandlerBinary(Socket s,String f,String c,int size){
		incoming=s;
		fileName=f;
		charSet=c;
		fileSize=size;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {

//				InputStream inStream = incoming.getInputStream();
				BufferedInputStream inStream = new BufferedInputStream(incoming.getInputStream());
                 System.out.println("BufferedInputStream");
				  int bufferLenth=1024*1024;
				  
			      byte[] data = new byte[fileSize];
			      int offset = 0;
//			      int bytesRead=0;
			      FileOutputStream fout = new FileOutputStream(fileName);
			      long s =System.currentTimeMillis();
			      while (offset < fileSize) {
			         int bytesRead = inStream.read(data, offset, data.length - offset);
			         if (bytesRead == -1) break;
			         offset += bytesRead;
			      }

//			      while (bytesRead != -1) {
//				         bytesRead = inStream.read(data, offset, data.length - offset);
//					        fout.write(data);
//				         offset += bytesRead;
//				      }
//			      
			      if (offset != fileSize) {
			        throw new IOException("Only read " + offset 
			            + " bytes; Expected " + fileSize + " bytes");
			      }
			      
			        fout.write(data);
			        fout.flush();
			    
                  System.out.println(System.currentTimeMillis()-s);
			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

class ReceiverHandlerBinary2 implements Runnable{
	private Socket incoming;
	private String fileName;
	private String charSet;
	private int fileSize;
	private int streamBufferLenth;
	public  ReceiverHandlerBinary2(Socket s,String f,String c,int size,int sbl){
		incoming=s;
		fileName=f;
		charSet=c;
		fileSize=size;
		streamBufferLenth=sbl;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {

//				InputStream inStream = incoming.getInputStream();
				BufferedInputStream inStream = new BufferedInputStream(incoming.getInputStream());
                 System.out.println("BufferedInputStream");
				 ByteArrayOutputStream baos = new ByteArrayOutputStream();
				 byte[] buffer = new byte[10];
				 int len=0;
				 while((len=inStream.read(buffer))>-1){
					 baos.write(buffer,0,len);
				 }
                 baos.flush();
                 InputStream in1;
                 InputStream in2;
//                 byte[] a1=baos.toByteArray();
//                 byte[] a2=new byte[a1.length];
//                 System.arraycopy(a1, 0, a2, 0, a1.length);
                 
                 in1= new ByteArrayInputStream(baos.toByteArray());
                 in2= new ByteArrayInputStream(baos.toByteArray());
                 
                 
			      byte[] data = new byte[streamBufferLenth];
			      int bytesRead=0;
			      int filesize=0;
			      FileOutputStream fout = new FileOutputStream(fileName+1);
			      long s =System.currentTimeMillis();
			      while ((bytesRead = in1.read(data))!=-1) {
			    	  filesize+=bytesRead;
			    	  System.out.println("speed:"+filesize);
                      fout.write(data,0,bytesRead);
			      }

	      
			        fout.flush();
			        fout.close();
			        System.out.println("write file1");
			        
				      byte[] data2 = new byte[streamBufferLenth];
				      int bytesRead2=0;
				      int filesize2=0;
				      FileOutputStream fout2 = new FileOutputStream(fileName+2);
				      long s2 =System.currentTimeMillis();
				      while ((bytesRead2 = in2.read(data2))!=-1) {
				    	  filesize2+=bytesRead2;
				    	  System.out.println("speed:"+filesize2);
	                      fout2.write(data2,0,bytesRead2);
				      }

		      
				        fout2.flush();
				        fout2.close();
				        System.out.println("write file2");
			    
                  System.out.println(System.currentTimeMillis()-s);
			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}


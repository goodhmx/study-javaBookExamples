package v2ch03.socket;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
	   public static void main(String[] args) throws IOException
	   {
	      // establish server socket
	      try 
	      {
	    	  ServerSocket s = new ServerSocket(8189);
	         // wait for client connection
	    	  while(true){
	    		  Socket incoming = s.accept();

		    	  Runnable r= new HandlerInputStream2File(incoming);		    	  
		    	  Thread t= new Thread(r);
		    	  t.start();
		    	  
//		    	  Runnable r2= new HandlerInputStream2File(incoming);		    	  
//		    	  Thread t2= new Thread(r2);
//		    	  t2.start();
	    	  }
//	            InputStream inStream = incoming.getInputStream();
//	            OutputStream outStream = incoming.getOutputStream();
//	   
//	            try (Scanner in = new Scanner(inStream))
//	            {
//	               PrintWriter out = new PrintWriter(outStream, true /* autoFlush */);
//	      
//	               out.println("Hello! Enter BYE to exit.");
//	      
//	               // echo client input
//	               boolean done = false;
//	               while (!done && in.hasNextLine())
//	               {
//	                  String line = in.nextLine();
//	                  out.println("Echo: " + line);
//	                  System.out.println(line);
//	                  if (line.trim().equals("BYE")) done = true;
//	               }
//	            }
	         }catch(IOException e){
	        	 e.printStackTrace();
	         }
	         
	   }
}
class HandlerInputStream2File implements Runnable{
	private Socket incoming;
	
	public  HandlerInputStream2File(Socket s){
		incoming=s;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName() + " : from :"
				+ incoming.getRemoteSocketAddress().toString());

		try {
			try {

				InputStream inStream = incoming.getInputStream();
				PrintWriter out = new PrintWriter("trans.log","UTF-8");
				FileOutputStream out1 = new FileOutputStream("trans1.log");

				out1.write(inStream.read());
				out1.flush();
				
				Scanner in = new Scanner(inStream);

				boolean done = false;
				while (!done && in.hasNextLine()) {
					String line = in.nextLine();
					out.println(line);
					if (line.trim().equals("BYE"))
						done = true;
				}

			} finally {
				
				incoming.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
class EchoInput implements Runnable{
	private Socket incoming;
	
	public  EchoInput(Socket s){
		incoming=s;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName()+" : from :"+incoming.getRemoteSocketAddress().toString());

		try{
        InputStream inStream=incoming.getInputStream();
        OutputStream outStream=incoming.getOutputStream();
        PrintWriter out=new PrintWriter(outStream,true);
        out.println("Hello! Enter BYE to exit.");
        Scanner in=new Scanner(inStream);

        boolean done=false;
        while(!done&&in.hasNextLine()){
        	String line= in.nextLine();
        	out.println("Echo:"+line);
        	if(line.trim().equals("BYE"))
        	done=true;
        }
        
		}catch (IOException e){
			e.printStackTrace();
		}
		
		

	}

}


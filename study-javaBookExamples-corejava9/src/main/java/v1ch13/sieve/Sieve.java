package v1ch13.sieve;

import java.util.*;

/**
 * This program runs the Sieve of Erathostenes benchmark. It computes all primes up to 2,000,000.
 * @version 1.21 2004-08-03
 * @author Cay Horstmann
 */
public class Sieve
{
   public static void main(String[] s)
   {
      int n = 10;
      long start = System.currentTimeMillis();
      BitSet b = new BitSet(n + 1);
      int count = 0;
      int i;
      for (i = 2; i <= n; i++)
         b.set(i);
      i = 2;
      while (i * i <= n)//计算sqrt(n)前面的数
      {
         if (b.get(i))
         {
            count++;
            int k = 2 * i;//
            while (k <= n)
            {
               b.clear(k);
               k += i;//i的所有倍数 从2倍开始
            }
         }
         i++;
      }
      while (i <= n)//计算sqrt(n)后面的数
      {
         if (b.get(i)) count++;
         i++;
      }
      long end = System.currentTimeMillis();
      System.out.println(count + " primes");
      System.out.println((end - start) + " milliseconds");
      System.out.println("b.length:"+b.length());
      System.out.println(b);
      //没有进行set或clear的位置，get返回值是 false
   }
}
